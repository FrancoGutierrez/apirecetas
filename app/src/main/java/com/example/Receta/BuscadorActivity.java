package com.example.Receta;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BuscadorActivity extends AppCompatActivity {

    private TextView tvNutricion, tvNutricion1, tvNutricion2, tvNutricion3, tvNutricion4, tvNutricion5, tvReceta;
    private Button btnAtras, btnBuscar;
    private EditText etBuscador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscador);

        this.tvReceta = (TextView) findViewById(R.id.tvReceta);
        this.etBuscador = (EditText) findViewById(R.id.etBuscador);
        this.btnBuscar = (Button) findViewById(R.id.btnBuscar);
        tvNutricion = (TextView) findViewById(R.id.tvNutricion);
        tvNutricion1 = (TextView) findViewById(R.id.tvNutricion1);
        tvNutricion2 = (TextView) findViewById(R.id.tvNutricion2);
        tvNutricion3 = (TextView) findViewById(R.id.tvNutricion3);
        tvNutricion4 = (TextView) findViewById(R.id.tvNutricion4);
        tvNutricion5 = (TextView) findViewById(R.id.tvNutricion5);
        btnAtras = (Button) findViewById(R.id.btnAtras);

        this.btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String clave = etBuscador.getText().toString();

                String url = "https://api.edamam.com/search?q=" + clave + "&app_id=4a0e061c&app_key=a761efa4d7cbd94cfdbadaa69ea25a9d";
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor
                                try {
                                    Log.i("Respuesta", response);
                                    JSONObject respuestaJSON = new JSONObject(response);

                                    JSONArray nutJSON = respuestaJSON.getJSONArray("hits");
                                    JSONObject nut2JSON = nutJSON.getJSONObject(0);
                                    JSONObject nut3JSON = nut2JSON.getJSONObject("recipe");
                                    JSONObject nut4JSON = nut3JSON.getJSONObject("totalNutrients");

                                    JSONObject nut5JSON = nut4JSON.getJSONObject("ENERC_KCAL");
                                    int energia = nut5JSON.getInt("quantity");

                                    tvNutricion.setText("Energia = " + energia + " kcal");

                                    JSONObject nut6JSON = nut4JSON.getJSONObject("FASAT");
                                    int saturada = nut6JSON.getInt("quantity");

                                    tvNutricion1.setText("G. saturada = " + saturada + " g");

                                    JSONObject nut7JSON = nut4JSON.getJSONObject("CHOCDF");
                                    int carbohidrato = nut7JSON.getInt("quantity");

                                    tvNutricion2.setText("Carbohidrato = " + carbohidrato + " g");

                                    JSONObject nut8JSON = nut4JSON.getJSONObject("SUGAR");
                                    int azucar = nut8JSON.getInt("quantity");

                                    tvNutricion3.setText("Azúcar = " + azucar + " g");

                                    JSONObject nut9JSON = nut4JSON.getJSONObject("PROCNT");
                                    int proteinas = nut9JSON.getInt("quantity");

                                    tvNutricion4.setText("Proteína = " + proteinas + " g");

                                    JSONObject nut10JSON = nut4JSON.getJSONObject("NA");
                                    int sodio = nut10JSON.getInt("quantity");

                                    tvNutricion5.setText("Sodio = " + sodio + " mg");


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Algo fallo
                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);


            }
        });

        this.btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });
    }
}
